# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the  package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: \n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2021-02-03 22:20+0000\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#: ../app/components/DifficultySelector.qml:18 ../app/ui/LevelSelector.qml:55
msgid "easy"
msgstr ""

#: ../app/components/DifficultySelector.qml:24 ../app/ui/LevelSelector.qml:56
msgid "medium"
msgstr ""

#: ../app/components/DifficultySelector.qml:30 ../app/ui/LevelSelector.qml:56
msgid "hard"
msgstr ""

#: ../app/components/ModalOverlay.qml:72 ../app/ui/UnlockTowerOverlay.qml:154
msgid "Close"
msgstr ""

#: ../app/components/PriceTag.qml:15
msgid "$"
msgstr ""

#: ../app/machines-vs-machines.qml:17
msgid "Machines vs. Machines"
msgstr ""

#: ../app/machines-vs-machines.qml:350
msgid "Please rotate your device"
msgstr ""

#: ../app/ui/GameView.qml:33
msgid "LEVEL:"
msgstr ""

#: ../app/ui/GameView.qml:39
msgid "WAVE:"
msgstr ""

#: ../app/ui/InfoPage.qml:34
msgid "Machines vs Machines"
msgstr ""

#: ../app/ui/InfoPage.qml:39
#, qt-format
msgid "Version: %1"
msgstr ""

#: ../app/ui/LevelPausedSplash.qml:11 ../app/ui/LevelResultsSplash.qml:22
#: ../app/ui/LevelSplash.qml:14
msgid "LEVEL"
msgstr ""

#: ../app/ui/LevelPausedSplash.qml:23
msgid "Restart level"
msgstr ""

#: ../app/ui/LevelPausedSplash.qml:30
msgid "Back to level selection"
msgstr ""

#: ../app/ui/LevelPausedSplash.qml:37
msgid "Back to main menu"
msgstr ""

#: ../app/ui/LevelResultsSplash.qml:44
msgid "New Best Score:"
msgstr ""

#: ../app/ui/LevelResultsSplash.qml:44
msgid "Score:"
msgstr ""

#: ../app/ui/LevelResultsSplash.qml:44
msgid "Failed"
msgstr ""

#: ../app/ui/LevelResultsSplash.qml:56 ../app/ui/LevelResultsSplash.qml:107
#: ../app/ui/ScoreInfo.qml:70
msgid "Okay"
msgstr ""

#: ../app/ui/LevelResultsSplash.qml:75
msgid "Retry"
msgstr ""

#: ../app/ui/LevelSplash.qml:34
msgid "Best Score: "
msgstr ""

#: ../app/ui/LevelSplash.qml:47
msgid "Play"
msgstr ""

#: ../app/ui/MainPage.qml:48
msgid "Play!"
msgstr ""

#: ../app/ui/ScoreInfo.qml:36
msgid "Levels: "
msgstr ""

#: ../app/ui/ScoreInfo.qml:43
msgid "Stars: "
msgstr ""

#: ../app/ui/ScoreInfo.qml:55
msgid "Reset"
msgstr ""

#: ../app/ui/Settings.qml:21
msgid "Sound settings"
msgstr ""

#: ../app/ui/Settings.qml:36
msgid "Sound"
msgstr ""

#: ../app/ui/Settings.qml:47
msgid "sound enabled"
msgstr ""

#: ../app/ui/Settings.qml:54
msgid "soundtrack volume"
msgstr ""

#: ../app/ui/Settings.qml:66
msgid "effects volume"
msgstr ""

#: ../app/ui/Settings.qml:87
msgid "Levelpack development"
msgstr ""

#: ../app/ui/Settings.qml:97
msgid "field overlay"
msgstr ""

#: ../app/ui/Settings.qml:108
msgid "all unlocked"
msgstr ""

#: ../app/ui/UnlockTowerOverlay.qml:65
msgid "Damage:"
msgstr ""

#: ../app/ui/UnlockTowerOverlay.qml:76
msgid "Slowdown:"
msgstr ""

#: ../app/ui/UnlockTowerOverlay.qml:86
msgid "Radius:"
msgstr ""

#: ../app/ui/UnlockTowerOverlay.qml:100
msgid "Shot duration:"
msgstr ""

#: ../app/ui/UnlockTowerOverlay.qml:103
msgid "ms"
msgstr ""

#: ../app/ui/UnlockTowerOverlay.qml:110
msgid "Shot recovery:"
msgstr ""

#: ../app/ui/UnlockTowerOverlay.qml:113
msgid " ms"
msgstr ""

#: ../app/ui/UnlockTowerOverlay.qml:120
msgid "Cost:"
msgstr ""

#: ../app/ui/UnlockTowerOverlay.qml:123
msgid " $"
msgstr ""

#: ../app/ui/UnlockTowerOverlay.qml:137
msgid "Unlock tower ("
msgstr ""

#: ../app/ui/UnlockTowerOverlay.qml:138
msgid "Get "
msgstr ""

#: ../app/ui/UnlockTowerOverlay.qml:138
msgid " more ⚝ to unlock!"
msgstr ""

#: ../app/ui/info/CreateLevelpack.qml:10
msgid "Not enough levels? Create your own!"
msgstr ""

#: ../app/ui/info/CreateLevelpack.qml:25
msgid "Creating level packs"
msgstr ""

#: ../app/ui/info/CreateLevelpack.qml:26
msgid "for Machines vs Machines is easy! All you need is to draw the"
msgstr ""

#: ../app/ui/info/CreateLevelpack.qml:27
msgid "artwork and create some simple text files that describe the levels."
msgstr ""

#: ../app/ui/info/CreateLevelpack.qml:28
msgid "The best submitted level packs will be included in this game!"
msgstr ""

#: ../app/ui/info/CreateLevelpack.qml:36
msgid "Interested?"
msgstr ""

#: ../app/ui/info/CreateLevelpack.qml:41
msgid "Visit the website http://notyetthere.org"
msgstr ""

#: ../app/ui/info/Credits.qml:10
msgid "Credits"
msgstr ""

#: ../app/ui/info/Credits.qml:26
msgid "Game development"
msgstr ""

#: ../app/ui/info/Credits.qml:30
msgid "Development"
msgstr ""

#: ../app/ui/info/Credits.qml:33
msgid "Design/Artwork"
msgstr ""

#: ../app/ui/info/Credits.qml:41
msgid "Level packs"
msgstr ""

#: ../app/ui/info/Donate.qml:10
msgid "Enjoying the game?"
msgstr ""

#: ../app/ui/info/Donate.qml:20
msgid "Send us a small <i><b>thank you</b>!</i>"
msgstr ""

#: ../app/ui/info/Donate.qml:27
msgid "Donate<br>via PayPal"
msgstr ""

#: ../app/ui/info/Feedback.qml:10
msgid "Let us know what you think!"
msgstr ""

#: ../app/ui/info/Feedback.qml:34
msgid "Leave us a review in the Open Store"
msgstr ""

#: ../app/ui/info/Feedback.qml:44
msgid "or"
msgstr ""

#: ../app/ui/info/Feedback.qml:58
msgid "Visit our Gitlab page to report a bug or contact us."
msgstr ""

#: ../app/ui/info/License.qml:10
msgid "Software license"
msgstr ""
