import QtQuick 2.0
import Ubuntu.Components 0.1

Item {
    id: root
    property alias model: listView.model
    property alias currentIndex: listView.currentIndex
    property string currentLevelPackId: listView.currentIndex > -1 ? listView.itemAt(listView.currentIndex).levelPackId : "no-item"

    Repeater {
        id: listView
        property int currentIndex: -1
        onCountChanged: root.currentLevelPackId = Qt.binding(function() { return listView.itemAt(listView.currentIndex).levelPackId;})

        delegate: Item {
            id: delegate
            height: root.height
            width: height * 2
            anchors.centerIn: parent
            state: ""
            z: 1
            scale: 0.8
            opacity: 0

            property string levelPackId: model.id

            states: [
                State {
                    name: "current"; when: index == listView.currentIndex
                    PropertyChanges { target: delegate; z: 200; scale: 1; opacity: 1 }
                    PropertyChanges { target: overlay; opacity: 0 }
                },
                State {
                    name: "previous"; when: index + 1 == listView.currentIndex
                    PropertyChanges { target: delegate; anchors.horizontalCenterOffset: -root.width / 4; z: 100; opacity: 1 }
                },
                State {
                    name: "next"; when: index - 1 == listView.currentIndex
                    PropertyChanges { target: delegate; anchors.horizontalCenterOffset: root.width / 4; z: 100; opacity: 1 }
                }
            ]
            BorderImage {
                anchors.fill: parent
                anchors.margins: -units.gu(1)
                opacity: 1
                source: "../graphics/dropshadow2gu.sci"
            }

            Rectangle {
                anchors.fill: parent
                color: "white"
            }

            Image {
                anchors.fill: parent
                anchors.margins: parent.height / 20
                source: "../../data/lpbuild/" + model.id + "/" + model.titleImage
                sourceSize.height: height
                sourceSize.width: width
            }

            transitions: [
                Transition {
                    from: "next"; to: "current"
                    ParallelAnimation {
                        PropertyAnimation { properties: "anchors.horizontalCenterOffset"; easing.type: Easing.InBack; easing.overshoot: 7.5; duration: UbuntuAnimation.SlowDuration }
                        PropertyAnimation { properties: "scale,opacity,z"; duration: UbuntuAnimation.SlowDuration }
                    }
                },
                Transition {
                    from: "previous"; to: "current"
                    ParallelAnimation {
                        PropertyAnimation { properties: "anchors.horizontalCenterOffset"; easing.type: Easing.InBack; easing.overshoot: 7.5; duration: UbuntuAnimation.SlowDuration }
                        PropertyAnimation { properties: "scale,opacity,z"; duration: UbuntuAnimation.SlowDuration }
                    }
                },
                Transition {
                    from: "current"
                    ParallelAnimation {
                        PropertyAnimation { properties: "anchors.horizontalCenterOffset"; easing.type: Easing.OutBack; easing.overshoot: 7.5; duration: UbuntuAnimation.SlowDuration }
                        PropertyAnimation { properties: "scale,opacity,z"; duration: UbuntuAnimation.SlowDuration }
                    }
                },
                Transition {
                    ParallelAnimation {
                        PropertyAnimation { properties: "anchors.horizontalCenterOffset,opacity,z"; duration: UbuntuAnimation.SlowDuration }
                    }
                }

            ]

            MouseArea {
                anchors.fill: parent
                onClicked: listView.currentIndex = index
            }

            Rectangle {
                id: overlay
                anchors.fill: parent
                color: "white"

                Icon {
                    anchors {
                        verticalCenter: parent.verticalCenter
                        left: parent.left
                        leftMargin: height / 4
                    }
                    height: parent.height / 4
                    width: height
                    color: "black"
                    name: "go-previous"
                }
                Icon {
                    anchors {
                        verticalCenter: parent.verticalCenter
                        right: parent.right
                        rightMargin: height / 4
                    }
                    height: parent.height / 4
                    width: height
                    color: "black"
                    name: "go-next"
                }
            }
        }
    }
}
