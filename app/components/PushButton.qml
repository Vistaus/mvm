import QtQuick 2.0
import Ubuntu.Components 0.1

AbstractButton {
    id: root
    implicitWidth: Math.max(app.buttonSize * 3, label.implicitWidth + app.margins * 2)
    height: app.buttonSize

    property color color: shape.color

    property alias text: label.text
    property alias textColor: label.color


    UbuntuShape {
        id: shape
        anchors.fill: parent
        color: root.pressed ?
                   Qt.rgba(root.color.r - .1, root.color.g - .1, root.color.b - .1, root.color.a)
                 : root.color
        Behavior on color { ColorAnimation { duration: UbuntuAnimation.SnapDuration } }
    }
    Label {
        id: label
        anchors.centerIn: parent
        font.pixelSize: root.height / 2
        color: app.buttonTextColor
    }
}
