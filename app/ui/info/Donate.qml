import QtQuick 2.2
import Ubuntu.Components 0.1
import Ubuntu.Components.ListItems 0.1
import "../../components"
import Machines 1.0
import QtQuick.Layouts 1.1

ModalOverlay {
    id: root
    title: i18n.tr("Enjoying the game?")
    showCloseButton: true

    Column {
        anchors.fill: parent
        anchors.margins: app.buttonSize
        spacing: app.margins

        GameLabel {
            anchors.horizontalCenter: parent.horizontalCenter
            text: i18n.tr("Send us a small <i><b>thank you</b>!</i>")
        }

        Button {
            height: parent.height - y
            width: (parent.width - app.buttonSize * 2)
            anchors.horizontalCenter: parent.horizontalCenter
            text: i18n.tr("Donate<br>via PayPal")
            color: app.confirmationButtonColor
            onClicked: Qt.openUrlExternally("https://www.paypal.com/cgi-bin/webscr?cmd=_s-xclick&hosted_button_id=USWAUUU2MD9Z4")
        }
    }
}
