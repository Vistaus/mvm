import QtQuick 2.2
import Ubuntu.Components 0.1
import Ubuntu.Components.ListItems 0.1
import "../../components"
import Machines 1.0
import QtQuick.Layouts 1.1

ModalOverlay {
    id: root
    title: i18n.tr("Not enough levels? Create your own!")
    showCloseButton: true

    Flickable {
        anchors.fill: parent
        anchors.margins: app.margins
        contentHeight: label.height
        clip: true

        Column {
            width: parent.width
            spacing: app.margins
            GameLabel {
                id: label
                width: parent.width
                text: i18n.tr("Creating level packs") + " " +
                      i18n.tr("for Machines vs Machines is easy! All you need is to draw the") + " " +
                      i18n.tr("artwork and create some simple text files that describe the levels.") + "<br>" +
                      i18n.tr("The best submitted level packs will be included in this game!")
                wrapMode: Text.WordWrap
            }
            Row {
                width: parent.width
                height: app.buttonSize

                GameLabel {
                    text: i18n.tr("Interested?")
                    anchors.verticalCenter: parent.verticalCenter
                }
                PushButton {
                    width: app.buttonSize * 6
                    text: i18n.tr("Visit the website http://notyetthere.org")
                    color: app.confirmationButtonColor
                    onClicked: Qt.openUrlExternally("http://notyetthere.org")
                }
            }
        }
    }
}
