import QtQuick 2.0
import QtQuick.Layouts 1.0
import Ubuntu.Components 0.1
import "../components"
import Machines 1.0

ModalOverlay {
    id: root
    color: "#77000000"

    property var lineup: null
    property int selectedLevel: 0


    Column {
        id: optionColumn
        anchors { left: parent.left; right: parent.right; top: parent.top; margins: app.margins }
        spacing: app.margins

        GameLabel {
            text: root.lineup ? root.lineup[root.selectedLevel].name : ""
            font.pixelSize: app.titleSize
            font.bold: true
            anchors.horizontalCenter: parent.horizontalCenter
        }

        Row {
            anchors.horizontalCenter: parent.horizontalCenter
            spacing: app.buttonSize / 2
            Repeater {
                model: root.lineup

                delegate: AbstractButton {
                    height: app.buttonSize * 2
                    width: app.buttonSize * 2
                    onClicked: root.selectedLevel = index

                    TowerDelegate {
                        anchors.fill: parent
                        source: Qt.resolvedUrl("../../data/lpbuild/" + root.lineup[index].image)
                        baseSize: root.lineup[index].imageBaseSize
                        spriteCount: root.lineup[index].spriteCount
                        duration: root.lineup[index].animationDuration
                        running: false
                        startFrame: 0
                        locked: root.lineup[index].locked
                        //cost: root.lineup[index].cost

                        opacity: index == root.selectedLevel ? 1 : 0.6
                    }
                }

            }
        }


        Row {
            anchors { left: parent.left; right: parent.right }
            spacing: app.margins * 4

            ColumnLayout {
                width: (parent.width - parent.spacing) / 2
                RowLayout {
                    GameLabel {
                        text: i18n.tr("Damage:")
                        Layout.fillWidth: true
                    }
                    GameLabel {
                        text: root.lineup ? root.lineup[root.selectedLevel].damage : ""
                        Layout.fillWidth: true
                        horizontalAlignment: Text.AlignRight
                    }
                }
                RowLayout {
                    GameLabel {
                        text: i18n.tr("Slowdown:")
                    }
                    GameLabel {
                        text: root.lineup ?  root.lineup[root.selectedLevel].slowdown : ""
                        Layout.fillWidth: true
                        horizontalAlignment: Text.AlignRight
                    }
                }
                RowLayout {
                    GameLabel {
                        text: i18n.tr("Radius:")
                    }
                    GameLabel {
                        text: root.lineup ? root.lineup[root.selectedLevel].radius : ""
                        Layout.fillWidth: true
                        horizontalAlignment: Text.AlignRight
                    }
                }
            }
            ColumnLayout {
                width: (parent.width - parent.spacing) / 2

                RowLayout {
                    GameLabel {
                        text: i18n.tr("Shot duration:")
                    }
                    GameLabel {
                        text: root.lineup ? root.lineup[root.selectedLevel].shotDuration + " " + i18n.tr("ms") : ""
                        Layout.fillWidth: true
                        horizontalAlignment: Text.AlignRight
                    }
                }
                RowLayout {
                    GameLabel {
                        text: i18n.tr("Shot recovery:")
                    }
                    GameLabel {
                        text: root.lineup ? root.lineup[root.selectedLevel].shotRecovery + i18n.tr(" ms") : ""
                        Layout.fillWidth: true
                        horizontalAlignment: Text.AlignRight
                    }
                }
                RowLayout {
                    GameLabel {
                        text: i18n.tr("Cost:")
                    }
                    GameLabel {
                        text: root.lineup ? root.lineup[root.selectedLevel].cost + i18n.tr(" $") : ""
                        Layout.fillWidth: true
                        horizontalAlignment: Text.AlignRight
                    }
                }
            }
        }
    }

    RowLayout {
        anchors { left: parent.left; right: parent.right; bottom: parent.bottom; margins: app.margins }
        spacing: app.margins
        PushButton {
            text: root.lineup ?
                      (enabled ? i18n.tr("Unlock tower (") + root.lineup[root.selectedLevel].unlockPoints + " ⚝)"
                          : i18n.tr("Get ") + (root.lineup[root.selectedLevel].unlockPoints - engine.points) + i18n.tr(" more ⚝ to unlock!"))
                    : ""
            enabled: root.lineup ? engine.points >= root.lineup[root.selectedLevel].unlockPoints : false
            color: enabled ? app.confirmationButtonColor : app.cancelButtonColor
            opacity: root.lineup ? (root.lineup[root.selectedLevel].locked ? 1 : 0) : 0
            onClicked: {
                engine.unlockTower(root.lineup[root.selectedLevel].index, root.lineup[root.selectedLevel].level)
                root.closeButtonClicked()
            }
        }
        Item {
            height: parent.height
            Layout.fillWidth: true
        }

        PushButton {
            text: i18n.tr("Close")
            color: app.playButtonColor
            width: app.buttonSize * 3
            onClicked: {
                root.closeButtonClicked()
            }
        }
    }
}
